module go.jolheiser.com/cherry

go 1.15

require (
	github.com/AlecAivazis/survey/v2 v2.2.9 // indirect
	github.com/go-git/go-git/v5 v5.2.0 // indirect
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966 // indirect
	github.com/urfave/cli/v2 v2.2.0
	go.jolheiser.com/beaver v1.0.2
	golang.org/x/sys v0.0.0-20200803210538-64077c9b5642 // indirect
)
