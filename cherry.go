package main

import (
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"regexp"
	"sort"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/skratchdot/open-golang/open"
)

var (
	branchDefaults = []string{"master", "main"}
)

func cherry() error {
	auth := &http.BasicAuth{
		Username: usernameFlag,
		Password: tokenFlag,
	}

	repo, err := git.PlainOpenWithOptions(".", &git.PlainOpenOptions{
		DetectDotGit: true,
	})
	if err != nil {
		return err
	}

	// Get upstream remote
	remoteUpstream, err := repo.Remote(remoteUpstreamFlag)
	if err != nil {
		return err
	}

	// Fetch if necessary
	if !noFetchFlag {
		if err := remoteUpstream.Fetch(&git.FetchOptions{
			Auth:       auth,
			RemoteName: remoteUpstreamFlag,
			RefSpecs: []config.RefSpec{
				config.DefaultFetchRefSpec,
			},
			Tags: git.AllTags,
		}); err != nil && err != git.NoErrAlreadyUpToDate {
			return err
		}
	}

	// Get branches from upstream
	refs, err := remoteUpstream.List(&git.ListOptions{
		Auth: auth,
	})
	if err != nil {
		return err
	}
	branchName := branchFromFlag
	branchOpts := make([]string, 0)
	branches := make(map[string]*plumbing.Reference)
	for _, ref := range refs {
		if ref.Name().IsBranch() {
			name := ref.Name().Short()
			if branchName == "" {
				for _, def := range branchDefaults {
					if strings.EqualFold(name, def) {
						branchName = def
					}
				}
			}
			branches[name] = ref
			branchOpts = append(branchOpts, name)
		}
	}
	sort.Strings(branchOpts)

	// Determine from-branch
	q := &survey.Select{
		Message: "Branch From",
		Options: branchOpts,
		Default: branchName,
	}
	if err := survey.AskOne(q, &branchName, survey.WithValidator(survey.Required)); err != nil {
		return err
	}
	branchFrom := branches[branchName]

	// Determine commit
	commitIter, err := repo.Log(&git.LogOptions{
		From: branchFrom.Hash(),
	})
	if err != nil {
		return err
	}
	commitOpts := make([]string, 0)
	commits := make(map[string]*object.Commit)
	if err := commitIter.ForEach(func(commit *object.Commit) error {
		msg := trunc(commit.Message)
		commits[msg] = commit
		commitOpts = append(commitOpts, msg)
		return nil
	}); err != nil {
		return err
	}
	q = &survey.Select{
		Message: "Commit",
		Options: commitOpts,
	}
	var commitMessage string
	if err := survey.AskOne(q, &commitMessage, survey.WithValidator(survey.Required)); err != nil {
		return err
	}
	commit := commits[commitMessage]

	// Determine to-branch
	q = &survey.Select{
		Message: "Branch To",
		Options: branchOpts,
	}
	if err := survey.AskOne(q, &branchName, survey.WithValidator(survey.Required)); err != nil {
		return err
	}
	branchTo := branches[branchName]

	tree, err := repo.Worktree()
	if err != nil {
		return err
	}

	// Backport name
	prDesc := commitMessage
	backportName := fmt.Sprintf("backport-%s", commit.Hash.String()[:8])
	prMatch := regexp.MustCompile(`\(#(\d+)\)`).FindStringSubmatch(commitMessage)
	if prMatch != nil {
		prDesc = fmt.Sprintf("Backport #%s", prMatch[1])
		backportName = fmt.Sprintf("backport-%s", prMatch[1])
	}
	qq := &survey.Input{
		Message: "Backport Name",
		Default: backportName,
	}
	if err := survey.AskOne(qq, &backportName, survey.WithValidator(survey.Required)); err != nil {
		return err
	}

	// Create new branch for backport
	if err := tree.Checkout(&git.CheckoutOptions{
		Hash:   branchTo.Hash(),
		Branch: plumbing.NewBranchReferenceName(backportName),
		Create: true,
		Force:  true,
	}); err != nil {
		return err
	}

	// Cherry-pick
	cmd := exec.Command("git", "cherry-pick", commit.Hash.String())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}

	// Push
	if err := repo.Push(&git.PushOptions{
		Auth:       auth,
		RemoteName: remoteOriginFlag,
		RefSpecs: []config.RefSpec{
			config.RefSpec(fmt.Sprintf("refs/heads/%s:refs/heads/%s", backportName, backportName)),
		},
	}); err != nil {
		return err
	}

	// Description
	qqq := &survey.Multiline{
		Message: "PR Description",
		Default: prDesc,
	}
	if err := survey.AskOne(qqq, &prDesc, survey.WithValidator(survey.Required)); err != nil {
		return err
	}

	// Open a PR
	compare := fmt.Sprintf("https://github.com/go-gitea/gitea/compare/%s...%s:%s?expand=1&body=%s",
		branchTo.Name().Short(), usernameFlag, backportName, url.QueryEscape(prDesc))
	return open.Run(compare)
}

func trunc(s string) string {
	idx := strings.Index(s, "\n")
	if idx == -1 {
		idx = len(s) - 1
	}
	return s[:idx]
}
