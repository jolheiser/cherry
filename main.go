package main

import (
	"flag"
	"os"

	"go.jolheiser.com/beaver"
)

var (
	noFetchFlag      bool
	remoteOriginFlag = func() string {
		u := os.Getenv("CHERRY_ORIGIN")
		if u == "" {
			return "origin"
		}
		return u
	}()
	remoteUpstreamFlag = func() string {
		u := os.Getenv("CHERRY_UPSTREAM")
		if u == "" {
			return "upstream"
		}
		return u
	}()
	branchFromFlag = os.Getenv("CHERRY_BRANCH_FROM")
	branchToFlag   = os.Getenv("CHERRY_BRANCH_TO")

	usernameFlag = os.Getenv("CHERRY_USERNAME")
	tokenFlag    = os.Getenv("CHERRY_TOKEN")
)

func main() {
	flag.BoolVar(&noFetchFlag, "no-fetch", false, "Don't fetch before running")
	flag.Func("origin", "Remote origin name", stringFlag(&remoteOriginFlag))
	flag.Func("upstream", "Remote upstream name", stringFlag(&remoteUpstreamFlag))
	flag.Func("branch-from", "From branch", stringFlag(&branchFromFlag))
	flag.Func("branch-to", "To branch", stringFlag(&branchToFlag))

	flag.Func("username", "Username", stringFlag(&usernameFlag))
	flag.Func("token", "Token", stringFlag(&tokenFlag))
	flag.Parse()

	if usernameFlag == "" {
		beaver.Fatal("--username is required")
	}

	if err := cherry(); err != nil {
		beaver.Fatal(err)
	}
}

func stringFlag(p *string) func(string) error {
	return func(s string) error {
		*p = s
		return nil
	}
}
